FROM node:14-alpine

RUN mkdir /home/user-node
RUN adduser user-node -h /home/user-node -D
RUN chown user-node.user-node /home/user-node

RUN mkdir -p /opt/app 
RUN chown user-node.user-node /opt/app
USER user-node
COPY . /opt/app/
WORKDIR /opt/app
RUN npm install
RUN npm run test
CMD node index.js